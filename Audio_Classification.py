# %%
import numpy as np
import librosa.display, os
import matplotlib.pyplot as plt
import warnings
from tensorflow.keras.models import load_model
from keras.preprocessing import image
from tensorflow.keras.applications.mobilenet import preprocess_input
from gtts import gTTS
import os




warnings.filterwarnings('ignore')


# %matplotlib inline

def create_spectrogram(audio_file, image_file):
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    fig.subplots_adjust(left=0, right=1, bottom=0, top=1)

    y, sr = librosa.load(audio_file)
    ms = librosa.feature.melspectrogram(y=y, sr=sr)
    log_ms = librosa.power_to_db(ms, ref=np.max)
    librosa.display.specshow(log_ms, sr=sr)

    fig.savefig(image_file)
    plt.close(fig)

# Function to preprocess spectrogram image
def preprocess_spectrogram(image_path):
    img = image.load_img(image_path, target_size=(224, 224))
    img_array = image.img_to_array(img)
    img_array = np.expand_dims(img_array, axis=0)
    img_array = preprocess_input(img_array)
    return img_array

def text_to_speech(text, filename):
    tts = gTTS(text=text, lang='kn')
    tts.save(filename)
    os.system(f'start {filename}')

def create_pngs_from_wavs(input_path, output_path):
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    dir = os.listdir(input_path)

    for i, file in enumerate(dir):
        input_file = os.path.join(input_path, file)
        print('input_file', input_file)
        output_file = os.path.join(output_path, file.replace('.wav', '.png'))
        create_spectrogram(input_file, output_file)


from sklearn.metrics import confusion_matrix
import seaborn as sns

sns.set()
model = load_model('audio1.h5')
create_spectrogram("C:/Users/User/Downloads/Tulu_Project/audios_tulu/bukka vishesha/APMC 86.wav", 'C:/Users/User/Downloads/Tulu_Project/Spectrograms/new_sample5.png')
create_spectrogram("C:/Users/User/Downloads/Tulu_Project/audios_tulu/bukka vishesha/APMC 86.wav", 'C:/Users/User/Downloads/Tulu_Project/Spectrograms/new_sample3.png')

# Preprocess and predict
preprocessed_image1 = preprocess_spectrogram('Spectrograms/new_sample5.png')
preprocessed_image2 = preprocess_spectrogram('Spectrograms/new_sample2.png')

predictions1 = model.predict(preprocessed_image1)
predictions2 = model.predict(preprocessed_image2)

class_labels = ['and vante vante barpund', 'bukka vishesha', 'eer_doora_povondullar', 'enk badvondund', 'ini enk mast sustavondund', 'maatergla solmelu', 'mast samaya aand tudu', 'tulu barpunde', 'vanas ande', 'yan kudlad baide']

# Print predictions for the first audio file
# Print predictions for the first audio file
# Get the label with the maximum average value for the first audio file
# Get the label with the maximum average value for the first audio file
max_label1 = class_labels[np.argmax(np.mean(predictions1, axis=0))]

# Get the label with the maximum average value for the second audio file
max_label2 = class_labels[np.argmax(np.mean(predictions2, axis=0))]

# Print the results
print("First audio file")
print(f'Max Average Label: {max_label1}')

# ... (Previous code remains unchanged)

# Check the max average label for the first audio file
if max_label1 == 'and vante vante barpund':
    speech_text = "Haudu svalpa svalpa bartaḍē"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'bukka vishesha':
    speech_text = "Matthe Vishesha"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'eer_doora_povondullar':
    speech_text = "Nivu ellige hoguttiddira"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'enk badvondund':
    speech_text = "Nanage Hasivagide"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'ini enk mast sustavondund':
    speech_text = "Indu nanu tumba daṇididdene"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'maatergla solmelu':
    speech_text = "Maatergla Solmelu"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'mast samaya aand tudu':
    speech_text = "Tumba Dina Aaiythu Nodadhe"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'tulu barpunde':
    speech_text = "Tulu Barthadha"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'vanas ande':
    speech_text = "Uta Aitha"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')
elif max_label1 == 'yan kudlad baide':
    speech_text = "Nanu kudladindha bande"
    print(speech_text)
    text_to_speech(speech_text, 'speech1.mp3')

os.system('start speech1.mp3')


